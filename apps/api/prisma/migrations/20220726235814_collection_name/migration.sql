/*
  Warnings:

  - You are about to alter the column `name` on the `Collection` table. The data in that column could be lost. The data in that column will be cast from `VarChar(45)` to `VarChar(15)`.

*/
-- AlterTable
ALTER TABLE "Collection" ALTER COLUMN "name" SET DATA TYPE VARCHAR(15);
