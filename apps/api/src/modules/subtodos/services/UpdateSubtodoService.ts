import { Subtodo } from '@prisma/client';
import { StatusCode } from 'status-code-enum';
import AppError from 'error/AppError';
import { ISubtodoRepository } from '../repository/ISubtodoRepository';

export class UpdateSubtodoService {
	constructor(private subtodoRepository: ISubtodoRepository) {}

	async execute(id: string, subtodo: Partial<Subtodo>) {
		const subtodoFound = await this.subtodoRepository.findById(id);

		if (!subtodoFound) {
			throw new AppError('Subtodo not found', StatusCode.ClientErrorNotFound);
		}

		return this.subtodoRepository.update(id, subtodo);
	}
}
