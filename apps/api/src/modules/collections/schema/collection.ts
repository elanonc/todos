import Joi from 'joi';

const NAME_MAX_LENGTH = 15;
const COLOR_MAX_LENGTH = 7;

const collectionSchema = Joi.object({
	name: Joi.string().required().max(NAME_MAX_LENGTH).messages({
		'any.required': 'Name is required',
		'string.max': 'Name must be less than 15 characters',
	}),
	color: Joi.string().required().max(COLOR_MAX_LENGTH).messages({
		'any.required': 'Color is required',
		'string.max': 'Color must be a valid hex color',
	}),
});

export default collectionSchema;
