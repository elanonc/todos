import { Collection } from '@prisma/client';
import { ICollectionRepository } from '../repository/ICollectionRepository';

class ListCollectionsService {
	constructor(private collectionRepository: ICollectionRepository) {}

	async execute(userId: string): Promise<Collection[]> {
		const collections = await this.collectionRepository.findByUserId(userId);

		const formattedCollections = collections.map(({ todos, ...rest }) => ({
			...rest,
			_count: {
				todos: todos.length,
				completedTodos: todos.filter(todo => todo.completed).length,
			},
		}));

		return formattedCollections;
	}
}

export default ListCollectionsService;
