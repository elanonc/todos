import { hash } from 'bcrypt';
import AppError from 'error/AppError';
import { StatusCode } from 'status-code-enum';
import {
	ICreateUserDTO,
	IUserRepository,
} from 'modules/user/repository/IUserRepository';

const BCRYPT_SALT_ROUNDS = 8;

class CreateUserService {
	constructor(private userRepository: IUserRepository) {}

	async execute(user: ICreateUserDTO) {
		const verifyEmail = await this.userRepository.findByEmail(user.email);

		if (verifyEmail) {
			throw new AppError(
				'Email already in use',
				StatusCode.ClientErrorBadRequest,
			);
		}

		const encryptedPassword = await hash(user.password, BCRYPT_SALT_ROUNDS);

		const createdUser = await this.userRepository.create({
			...user,
			password: encryptedPassword,
		});

		return createdUser;
	}
}

export default CreateUserService;
