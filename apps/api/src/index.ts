import 'express-async-errors';
import express from 'express';
import cors from 'cors';

import { catchError } from 'middlewares/catchError';
import dotenv from 'dotenv';
import dotenvExpand from 'dotenv-expand';
import routes from './routes';

const myEnv = dotenv.config();
dotenvExpand.expand(myEnv);

const PORT = process.env.PORT;

export const app = express();

app.use(cors());
app.use(express.json());
app.use(routes);

app.use(catchError);

if (process.env.NODE_ENV !== 'test') {
	app.listen(PORT, () => {
		// eslint-disable-next-line no-console
		console.log(`Server is running on port ${PORT}`);
	});
}
