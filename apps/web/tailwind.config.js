/** @type {import('tailwindcss').Config} */
module.exports = {
	content: [
		'./src/pages/**/*.{js,ts,jsx,tsx}',
		'./src/components/**/*.{js,ts,jsx,tsx}',
		'./src/templates/**/*.{js,ts,jsx,tsx}',
	],
	theme: {
		fontFamily: {
			sans: ['Inter', 'sans-serif'],
		},
		container: {
			center: true,
		},
		extend: {
			colors: {
				main: {
					600: '#414052',
					700: '#272732',
					750: '#20212c',
					800: '#21212b',
					850: '#1d1d26',
					900: '#181820',
				},
				secondary: '#f96ca7',
			},
		},
	},
	plugins: [require('@tailwindcss/forms')],
};
