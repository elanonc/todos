<h1 align='center'>todos-web</h1>

## 🛠 Technologies

This project was developed with the following technologies:

Frontend

- [Turborepo](https://turborepo.org/)
- [Jest](https://jestjs.io/pt-BR/)
- [Testing Library](https://testing-library.com/)
- [React Query](https://react-query.tanstack.com/)
- [Tailwind](https://tailwindcss.com/)
- [ReactJS](https://pt-br.reactjs.org)
- [Next.js](https://nextjs.org)
- [Typescript](typescriptlang.org/)

## 🤔 How to contribute

- Fork this repository;
- Create a branch with your feature: `git checkout -b my-feature`;
- Commit your changes: `git commit -m 'feat: My new feature'`;
- Push to your branch: `git push origin my-feature`.

Once your pull request has been merged, you can delete your branch.
