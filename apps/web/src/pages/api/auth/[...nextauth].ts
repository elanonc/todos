import NextAuth, { NextAuthOptions } from 'next-auth';
import CredentialsProvider from 'next-auth/providers/credentials';
import { signIn } from 'api';

export const authOptions: NextAuthOptions = {
	providers: [
		CredentialsProvider({
			credentials: {
				email: { label: 'Email', type: 'text', placeholder: 'jsmith' },
				password: { label: 'Password', type: 'password' },
			},
			async authorize(credentials) {
				if (!credentials) {
					return null;
				}

				const signInResponse = await signIn({
					email: credentials.email,
					password: credentials.password,
				});

				if (signInResponse.token && signInResponse) {
					return {
						...signInResponse.user,
						token: signInResponse.token,
					};
				}

				return null;
			},
		}),
	],
	secret: process.env.NEXTAUTH_SECRET,
	session: {
		strategy: 'jwt',
	},
	pages: {
		signIn: '/login',
	},
	callbacks: {
		jwt: async ({ token, user }) => {
			if (user) {
				token.sub = JSON.stringify(user);
			}

			return token;
		},
		session: async ({ session, token }) => {
			if (token.sub) {
				session.user = JSON.parse(token.sub);
			}

			return session;
		},
	},
};

export default NextAuth(authOptions);
