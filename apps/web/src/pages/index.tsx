import { Bubble } from 'components/Bubble';
import Link from 'next/link';

const Home = () => (
	<div className="container px-8">
		<header className="flex h-28 items-center justify-between">
			<h1 className="text-2xl font-semibold">todos.</h1>
			<nav>
				<Link href="/login">
					<a className="mr-2 rounded-xl px-6 py-2 duration-300 ease-in-out hover:bg-main-600">
						Log in
					</a>
				</Link>
				<Link href="/sign-in">
					<a className="rounded-xl border-2 border-main-600 px-6 py-2">
						Sign in
					</a>
				</Link>
			</nav>
		</header>
		<main>
			<section className="mt-40 flex flex-col items-center justify-center">
				<h2 className="text-6xl font-bold">Just do it</h2>
				<p className="my-4 text-center text-zinc-400">
					Keep track of the daily tasks in life and get that
					<br />
					satisfaction upon completion.
				</p>
				<Link href="/sign-in">
					<a className="gradient-bg mt-4 rounded-xl px-8 py-3 font-medium">
						Get started
					</a>
				</Link>
			</section>
		</main>
		<Bubble variant="BLACK" size={160} right={200} top={140} />
		<Bubble size={140} top={300} />
		<Bubble size={200} bottom={200} right={120} />
	</div>
);

export default Home;
