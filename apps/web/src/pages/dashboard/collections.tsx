import { FiPlus } from 'react-icons/fi';
import { CollectionCard } from 'components/CollectionCard';
import { Header } from 'components/Header';

const Collections = () => (
	<>
		<Header />
		<main className="mx-auto max-w-xl">
			<h1 className="my-12 text-4xl font-bold leading-snug">Collections</h1>

			<section className="grid grid-cols-3 gap-4">
				<CollectionCard />
				<button
					type="button"
					className="border-2 border-main-600 border-dashed flex items-center justify-center rounded-2xl hover:border-main-600/50 text-main-600 hover:text-main-600/50"
				>
					<FiPlus className="w-6 h-6" />
				</button>
			</section>
		</main>
	</>
);

export default Collections;
