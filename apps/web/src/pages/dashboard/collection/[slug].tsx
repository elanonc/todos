import { useRouter } from 'next/router';
import { FiChevronLeft, FiPlus } from 'react-icons/fi';
import { Header } from 'components/Header';
import { MyLink } from 'components/common/MyLink';
import { Todo } from 'components/Todo';

const Collection = () => {
	const { query } = useRouter();
	return (
		<>
			<Header />
			<main className="mx-auto max-w-xl">
				<div className="flex items-center">
					<MyLink
						href="/dashboard/collections"
						className="bg-main-800 hover:bg-main-700 p-4 rounded-2xl pointer mr-6"
					>
						<FiChevronLeft className="w-6 h-6" />
					</MyLink>
					<h1 className="my-12 text-4xl font-bold leading-snug">
						{query.slug}
					</h1>
				</div>

				<button
					type="button"
					className="px-4 py-2 rounded-2xl border-2 border-main-750 hover:border-main-700 flex items-center w-full"
				>
					<div className="p-2 bg-secondary rounded-xl flex items-center justify-center">
						<FiPlus className="w-4 h-4 text-main-700" />
					</div>
					<p className="ml-4 text-neutral-400">Add a task</p>
				</button>

				<section className="flex flex-col mt-12 space-y-2">
					<Todo
						label="Todo title"
						id="todo-1"
						subtodos={[
							{
								id: 'todo-1',
								label: 'Subtodo 1',
							},
						]}
					/>
					<Todo label="Todo title 2" id="todo-2" />
				</section>
			</main>
		</>
	);
};

export default Collection;
