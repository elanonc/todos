import { ButtonHTMLAttributes } from 'react';
import { IconType } from 'react-icons';
import classNames from 'classnames';

enum ButtonVariants {
	BLACK = 'bg-main-900 border-main-700 border-2',
	GRADIENT = 'gradient-bg',
}

type ButtonProps = {
	icon?: IconType;
	text?: string;
	variant?: keyof typeof ButtonVariants;
} & ButtonHTMLAttributes<HTMLButtonElement>;

const Button = ({
	text,
	variant = 'BLACK',
	icon: Icon,
	className,
	...rest
}: ButtonProps) => (
	<button
		type="button"
		className={`flex h-full w-full items-center justify-center rounded-lg py-2 font-semibold text-neutral-300 drop-shadow-lg ${ButtonVariants[variant]} ${className}`}
		{...rest}
	>
		{Icon && (
			<Icon
				className={classNames({
					'mr-2': text,
				})}
			/>
		)}
		{text}
	</button>
);

export { Button };
