import { forwardRef, InputHTMLAttributes, useState } from 'react';
import { IconType } from 'react-icons';
import { FiAlertCircle, FiEye, FiEyeOff } from 'react-icons/fi';

type InputProps = {
	icon?: IconType;
	errorMessage?: string;
} & InputHTMLAttributes<HTMLInputElement>;

const Input = forwardRef<HTMLInputElement, InputProps>(
	({ icon: Icon, errorMessage, ...rest }: InputProps, ref) => {
		const [canSeePassword, setCanSeePassword] = useState(false);
		return (
			<div>
				<div className="text-sm flex items-center rounded-lg bg-main-800 border border-main-600 focus-within:ring-2 ring-pink-300">
					{Icon && (
						<div aria-hidden className="p-4">
							<Icon />
						</div>
					)}
					<input
						ref={ref}
						className="h-full w-full grow rounded-sm border-none bg-transparent py-2 pl-3 focus:outline-0 focus:ring-0"
						{...rest}
						type={canSeePassword ? 'text' : rest.type}
					/>
					{rest.type === 'password' && (
						<button
							type="button"
							className="rounded-sm p-4"
							onClick={() => setCanSeePassword(prevState => !prevState)}
						>
							{canSeePassword ? <FiEye /> : <FiEyeOff />}
						</button>
					)}
				</div>
				{errorMessage && (
					<span className="flex items-center mt-2 text-rose-500">
						<FiAlertCircle className="w-4 h-4 mr-2" />
						{errorMessage}
					</span>
				)}
			</div>
		);
	},
);

Input.displayName = 'Input';

export { Input };
