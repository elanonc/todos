import classNames from 'classnames';

type BubbleProps = {
	size: number;
	variant?: keyof typeof Variants;
	top?: number | string;
	left?: number | string;
	right?: number | string;
	bottom?: number | string;
};

enum Variants {
	COLORED = 'gradient-bg',
	BLACK = 'bg-gradient-to-tr from-main-900 via-main-800 to-main-600',
}

const Bubble = ({ variant = 'COLORED', size, ...rest }: BubbleProps) => (
	<div
		aria-hidden="true"
		style={{ ...rest, height: size, width: size }}
		className={classNames(
			'rounded-full absolute invisible md:visible -z-10',
			Variants[variant],
		)}
	></div>
);

export { Bubble };
