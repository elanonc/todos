import { Popover, Transition } from '@headlessui/react';
import { Fragment } from 'react';
import { FiPlus } from 'react-icons/fi';
import { Button } from './common/Button';
import { Input } from './common/Input';
import { Select } from './common/Select';

const AddTodoPopover = () => (
	<Popover className="relative">
		<Popover.Button className="gradient-bg rounded-lg p-2">
			<span className="sr-only">Add Todo</span>
			<FiPlus className="w-4 h-4" aria-hidden="true" />
		</Popover.Button>

		<Transition
			as={Fragment}
			enter="transition ease-out duration-200"
			enterFrom="opacity-0 translate-y-1"
			enterTo="opacity-100 translate-y-0"
			leave="transition ease-in duration-150"
			leaveFrom="opacity-100 translate-y-0"
			leaveTo="opacity-0 translate-y-1"
		>
			<Popover.Panel className="absolute right-0 top-14 w-screen max-w-sm">
				<div className="bg-main-800 rounded-md border border-main-600 p-4">
					<div className="flex flex-col gap-4">
						<span className="text-xl font-medium">Add new todo</span>
						<form className="flex flex-col gap-2">
							<Input placeholder="Todo title" />
							<Select />

							<Button
								variant="GRADIENT"
								text="Create"
								className="w-24 self-end mt-4"
							/>
						</form>
					</div>
				</div>
			</Popover.Panel>
		</Transition>
	</Popover>
);

export { AddTodoPopover };
