import { CreateUserProps } from 'pages/sign-in';

const url = 'http://localhost:3001/api/v1';

const fetchJson = async <T>(
	path: string,
	options: RequestInit = {},
): Promise<T> => {
	const res = await fetch(`${url}${path}`, {
		...options,
		headers: {
			'Content-Type': 'application/json',
			...options.headers,
		},
	});

	return res.json();
};

export const getCollections = async () => fetchJson('/collections');

export const createUser = async (
	body: Omit<CreateUserProps, 'confirm_password'>,
) =>
	fetchJson('/user', {
		method: 'POST',
		body: JSON.stringify(body),
	});

type SignInResponse = {
	token: string;
	user: {
		id: string;
		createdAt: string;
		name: string;
		email: string;
	};
};

export const signIn = async (body: {
	email: string;
	password: string;
}): Promise<SignInResponse> => {
	const { email, password } = body;
	return fetchJson('/session', {
		method: 'POST',
		body: JSON.stringify({ email, password }),
	});
};
