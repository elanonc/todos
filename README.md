<h1 align='center'>🚧 Under construction 🚧</h1>

<h1 align='center'>tasks</h1>
<p align='center'>A todo list app that I'm building to practice my skills building an entire app by myself</p>

<h1 align="center">MODIFICACOES P TRABALHO FINAL DE GC<h1>>

## 🛠 Technologies

This project was developed with the following technologies:

Frontend

- [Turborepo](https://turborepo.org/)
- [Jest](https://jestjs.io/pt-BR/)
- [Testing Library](https://testing-library.com/)
- [React Query](https://react-query.tanstack.com/)
- [Tailwind](https://tailwindcss.com/)
- [ReactJS](https://pt-br.reactjs.org)
- [Next.js](https://nextjs.org)
- [Typescript](typescriptlang.org/)

Backend

- [Turborepo](https://turborepo.org/)
- [Jest](https://jestjs.io/pt-BR/)
- [NodeJS](https://nodejs.org/)
- [Typescript](https://typescriptlang.org/)
- [Express](http://expressjs.com/pt-br/)
- [Prisma](https://www.prisma.io/)
- [Postgresql](https://www.postgresql.org/)
- [Docker](https://www.docker.com)

## 📱💻 Instructions

```bash
## 1. Clone repo
git clone https://github.com/guivictorr/todos.git

## 2. Change to project folder
cd todos

## 3. Install dependencies
yarn

## 4. Configure .env file with your database credentials
cd apps/api # configure your .env file based on .env.example

## 5. Run migrations
cd apps/api && npx prisma migrate dev

## 6. Start the back and front
# on root of the project run this command to start back and front together
yarn dev
```

## 🐳 Docker

To run this application with docker, after configure `.env` file and before run `yarn dev` you need to run `docker-compose up -d` to create and start a container running a postgres image

## 🤔 How to contribute

- Fork this repository;
- Create a branch with your feature: `git checkout -b my-feature`;
- Commit your changes: `git commit -m 'feat: My new feature'`;
- Push to your branch: `git push origin my-feature`.

Once your pull request has been merged, you can delete your branch.

## 📝 License

This project is under the MIT license. See the [LICENSE](https://github.com/guivictorr/todos/blob/master/LICENSE.md) file for more details.
